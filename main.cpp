#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "map.h"
#include "view.h"
#include "history.h"
#include <iostream>
#include <sstream>
#include "menu.h"
#include "health.h"

using namespace sf;

class Entity
{
public:
	float x, y = 0, w, h, dx, dy, speed;
	int dir, health, heal;
	bool life;
	String File;
	Image image;
	Texture texture;
	Sprite sprite;

	Entity(int heal, String F, float X, float Y, float W, float H)
	{
		dx = 0; dy = 0; dir = 0; speed = 0; health = heal;
		life = true;
		File = F;
		x = X; y = Y;
		w = W; h = H;
		image.loadFromFile("Images/" + File);
		texture.loadFromImage(image);
		sprite.setTexture(texture);
	}
};

class Player : public Entity
{
public:
	int health_count = 0;
        int episode = 1;
	bool knife = false;
	Player(int heal, String F, float X, float Y, float W, float H) : Entity(heal, F, X, Y, W, H)
	{
		sprite.setScale(0.6f, 0.6f);
		sprite.setTextureRect(IntRect(0, 0, w, h));
	}

	void update(float time)
	{

		switch (dir)
		{
		case 0: dx = speed; dy = 0; break;
		case 1: dx = -speed; dy = 0; break;
		case 2: dx = 0; dy = speed; break;
		case 3: dx = 0; dy = -speed; break;
		}
		x += dx * time;
		y += dy * time;
		speed = 0;
		sprite.setPosition(x, y);
		interactionWithMap();

		if (health <= 0) { life = false; }
	}

	float getPlayerCoordinateX() { return x; }
	float getPlayerCoordinateY() { return y; }

	void interactionWithMap()
	{
		for (int i = y / 32; i < (y + h) / 32; i++)
			for (int j = x / 32; j < (x + w) / 32; j++)
			{
				if (TileMap[i][j] == 's' || TileMap[i][j] == ',' ||
					TileMap[i][j] == 'c' || TileMap[i][j] == 'v' ||
					TileMap[i][j] == 'd' || TileMap[i][j] == 'a' ||
					TileMap[i][j] == 'z' || TileMap[i][j] == 'l' ||
					TileMap[i][j] == '0' || TileMap[i][j] == 'o' ||
					TileMap[i][j] == '[' || TileMap[i][j] == ']' ||
					TileMap[i][j] == 'p' || TileMap[i][j] == ';' ||
					TileMap[i][j] == 'f' || TileMap[i][j] == 'j' ||
					TileMap[i][j] == '.' || TileMap[i][j] == '5' ||
					TileMap[i][j] == '!' || TileMap[i][j] == '?' ||
					TileMap[i][j] == '`' || TileMap[i][j] == '|' ||
					TileMap[i][j] == 'Q' || TileMap[i][j] == 'W' ||
					TileMap[i][j] == 'E' || TileMap[i][j] == 'T')
				{
					if (dy > 0) { y = i * 32 - h; }
					if (dy < 0) { y = i * 32 + 32; }
					if (dx > 0) { x = j * 32 - w; }
					if (dx < 0) { x = j * 32 + 32; }
				}
				if (TileMap[i][j] == 'h')
				{
					health_count++;
					TileMap[i][j] = 'x';
				}
				if (TileMap[i][j] == 'H')
				{
					health_count++;
					TileMap[i][j] = ' ';
				}
				if (TileMap[i][j] == 'J')
				{
					health_count++;
					TileMap[i][j] = 'u';
				}
				if (TileMap[i][j] == 'M')
				{
					knife = true;
					TileMap[i][j] = '/';
                                        episode = 2;
				}
			}
	}
	void battle(int &healthE, float &cooldown, float Ex, float Ey) 
	{
		if (knife)
			if (cooldown > 150) 
			{
				if ((int(Ex) - 20) <= int(getPlayerCoordinateX()) && (int(Ey) - 20) <= int(getPlayerCoordinateY()) && (int(Ex) + 20) >= int(getPlayerCoordinateX()) && (int(Ey) + 20) >= int(getPlayerCoordinateY())) 
				{
					healthE -= 5;
					cooldown = 0;
				}
			}
	}
};

class EnemyZombi :public Entity 
{
public:
	float cooldownTime = 0;
	EnemyZombi(int heal, String F, float X, float Y, int W, int H) :Entity(heal, F, X, Y, W, H) 
	{
		sprite.setTextureRect(IntRect(0, 0, w, h));
	}

	void AreaVisibility(float &currentFrame, float cooldown, int &health, float PlayerCoordinateX, float PlayerCoordinateY, float time, int MixAreaVisibilityX, int MixAreaVisibilityY, int MaxAreaVisibilityX, int MaxAreaVisibilityY, int StartingPositionX, int StartingPositionY) {
		if (life) 
		{
			if ((PlayerCoordinateX < MaxAreaVisibilityX && PlayerCoordinateY < MaxAreaVisibilityY) && (MixAreaVisibilityX < PlayerCoordinateX && MixAreaVisibilityY < PlayerCoordinateY)) 
			{
				cooldownTime += time;
				battle(health, cooldown, PlayerCoordinateX, PlayerCoordinateY);
				update(currentFrame, time, PlayerCoordinateX, PlayerCoordinateY);
			}
			else
			{
				update(currentFrame, time, StartingPositionX, StartingPositionY);
			}
		}
	}

	void update(float &currentFrame, float time, float px, float py)
	{
		if (int(x) > int(px)) 
		{
			dx = -0.1; dy = 0;
			currentFrame += 0.005 * time;
			if (currentFrame > 3) currentFrame -= 3;
			sprite.setTextureRect(IntRect(200, 51 * int(currentFrame), w, h));
		}
		if (int(x) < int(px))
		{
			dx = 0.1; dy = 0;
			currentFrame += 0.005*time;
			if (currentFrame > 3) currentFrame -= 3;
			sprite.setTextureRect(IntRect(250, 51 * int(currentFrame), w, h));
		}
		if (int(y) > int(py)) 
		{
			dx = 0; dy = -0.1;
			currentFrame += 0.005 * time;
			if (currentFrame > 2) currentFrame -= 2;
			sprite.setTextureRect(IntRect(103, 51 * int(currentFrame), w, h));
		}
		if (int(y) < int(py)) 
		{
			dx = 0; dy = 0.1;
			currentFrame += 0.005 * time;
			if (currentFrame > 3) currentFrame -= 3;
			sprite.setTextureRect(IntRect(0, 49 * int(currentFrame), w, h));
		}
		if (int(y) == int(py) && int(x) == int(px)) 
		{
			dx = 0; dy = 0;
			sprite.setTextureRect(IntRect(0, 49, w, h));
		}
		x += dx * time;
		y += dy * time;
		sprite.setPosition(x, y);
		checkCollisionWithMap();
		if (health <= 0) { life = false; }
	}

	float getPlayerCoordinateX() { return x; }
	float getPlayerCoordinateY() { return y; }

	void checkCollisionWithMap()
	{
		for (int i = y / 32; i < (y + h) / 32; i++)
			for (int j = x / 32; j<(x + w) / 32; j++)
			{
				if (TileMap[i][j] == 's' || TileMap[i][j] == ',' ||
					TileMap[i][j] == 'c' || TileMap[i][j] == 'v' ||
					TileMap[i][j] == 'd' || TileMap[i][j] == 'a' ||
					TileMap[i][j] == 'z' || TileMap[i][j] == 'l' ||
					TileMap[i][j] == '0' || TileMap[i][j] == 'o' ||
					TileMap[i][j] == '[' || TileMap[i][j] == ']' ||
					TileMap[i][j] == 'p' || TileMap[i][j] == ';' ||
					TileMap[i][j] == 'f' || TileMap[i][j] == 'j' ||
					TileMap[i][j] == '.' || TileMap[i][j] == '5' ||
					TileMap[i][j] == '!' || TileMap[i][j] == '?' ||
					TileMap[i][j] == '`' || TileMap[i][j] == '|' ||
					TileMap[i][j] == 'Q' || TileMap[i][j] == 'W' ||
					TileMap[i][j] == 'E' || TileMap[i][j] == 'T')
				{
					if (dy > 0) { y = i * 32 - h; }
					if (dy < 0) { y = i * 32 + 32; }
					if (dx > 0) { x = j * 32 - w; }
					if (dx < 0) { x = j * 32 + 32; }
				}
			}
	}

	void battle(int &healthP, float &cooldown, float Ex, float Ey) 
	{
		if (cooldownTime > cooldown) 
		{
			if ((int(Ex) - 20) <= int(getPlayerCoordinateX()) && (int(Ey) - 20) <= int(getPlayerCoordinateY()) && (int(Ex) + 20) >= int(getPlayerCoordinateX()) && (int(Ey) + 20) >= int(getPlayerCoordinateY())) 
			{
				healthP -= 5;
				cooldownTime = 0;
			}
		}
	}
};

class EnemyVolf :public Entity 
{
public:
	float cooldownTime = 0;
	EnemyVolf(int heal, String F, float X, float Y, int W, int H) :Entity(heal, F, X, Y, W, H) 
	{
		sprite.setTextureRect(IntRect(0, 0, w, h));
	}

	void AreaVisibility(float &currentFrame, float cooldown, int &health, float PlayerCoordinateX, float PlayerCoordinateY, float time, int MixAreaVisibilityX, int MixAreaVisibilityY, int MaxAreaVisibilityX, int MaxAreaVisibilityY, int StartingPositionX, int StartingPositionY) 
	{
		if (life) 
		{
			if ((PlayerCoordinateX < MaxAreaVisibilityX && PlayerCoordinateY < MaxAreaVisibilityY) && (MixAreaVisibilityX < PlayerCoordinateX && MixAreaVisibilityY < PlayerCoordinateY)) 
			{
				cooldownTime += time;
				battle(health, cooldown, PlayerCoordinateX, PlayerCoordinateY);
				update(currentFrame, time, PlayerCoordinateX, PlayerCoordinateY);
			}
			else
			{
				update(currentFrame, time, StartingPositionX, StartingPositionY);
			}
		}
	}

	void update(float &currentFrame, float time, float px, float py)
	{
		if (int(x) > int(px)) 
		{
			dx = -0.1; dy = 0;
			currentFrame += 0.005 * time;
			if (currentFrame > 3) currentFrame -= 3;
			sprite.setTextureRect(IntRect(96 * int(currentFrame), 96, w, h));
		}
		if (int(x) < int(px)) 
		{
			dx = 0.1; dy = 0;
			currentFrame += 0.005*time;
			if (currentFrame > 3) currentFrame -= 3;
			sprite.setTextureRect(IntRect(96 * int(currentFrame), 192, w, h));
		}
		if (int(y) > int(py)) 
		{
			dx = 0; dy = -0.1;
			currentFrame += 0.005 * time;
			if (currentFrame > 3) currentFrame -= 3;
			sprite.setTextureRect(IntRect(96 * int(currentFrame), 288, w, h));
		}
		if (int(y) < int(py)) 
		{
			dx = 0; dy = 0.1;
			currentFrame += 0.005 * time;
			if (currentFrame > 3) currentFrame -= 3;
			sprite.setTextureRect(IntRect(96 * int(currentFrame), 0, w, h));
		}
		if (int(y) == int(py) && int(x) == int(px)) 
		{
			dx = 0; dy = 0;
			sprite.setTextureRect(IntRect(96, 0, w, h));
		}
		x += dx * time;
		y += dy * time;
		sprite.setPosition(x, y);
		checkCollisionWithMap();
		if (health <= 0) { life = false; }
	}

	float getPlayerCoordinateX() { return x; }
	float getPlayerCoordinateY() { return y; }

	void checkCollisionWithMap()
	{
		for (int i = y / 32; i < (y + h) / 32; i++)
			for (int j = x / 32; j<(x + w) / 32; j++)
			{
				if (TileMap[i][j] == 's' || TileMap[i][j] == ',' ||
					TileMap[i][j] == 'c' || TileMap[i][j] == 'v' ||
					TileMap[i][j] == 'd' || TileMap[i][j] == 'a' ||
					TileMap[i][j] == 'z' || TileMap[i][j] == 'l' ||
					TileMap[i][j] == '0' || TileMap[i][j] == 'o' ||
					TileMap[i][j] == '[' || TileMap[i][j] == ']' ||
					TileMap[i][j] == 'p' || TileMap[i][j] == ';' ||
					TileMap[i][j] == 'f' || TileMap[i][j] == 'j' ||
					TileMap[i][j] == '.' || TileMap[i][j] == '5' ||
					TileMap[i][j] == '!' || TileMap[i][j] == '?' ||
					TileMap[i][j] == '`' || TileMap[i][j] == '|' ||
					TileMap[i][j] == 'Q' || TileMap[i][j] == 'W' ||
					TileMap[i][j] == 'E' || TileMap[i][j] == 'T')
				{
					if (dy > 0) { y = i * 32 - h; }
					if (dy < 0) { y = i * 32 + 32; }
					if (dx > 0) { x = j * 32 - w; }
					if (dx < 0) { x = j * 32 + 32; }
				}
			}
	}

	void battle(int &healthP, float &cooldown, float Ex, float Ey) 
	{
		if (cooldownTime > cooldown) 
		{
			if ((int(Ex) - 20) <= int(getPlayerCoordinateX()) && (int(Ey) - 20) <= int(getPlayerCoordinateY()) && (int(Ex) + 20) >= int(getPlayerCoordinateX()) && (int(Ey) + 20) >= int(getPlayerCoordinateY())) 
			{
				healthP -= 5;
				cooldownTime = 0;
			}
		}
	}
};

class EnemyKiller :public Entity 
{
public:
	float cooldownTime = 0;
	EnemyKiller(int heal, String F, float X, float Y, int W, int H) :Entity(heal, F, X, Y, W, H) 
	{
		sprite.setTextureRect(IntRect(0, 0, w, h));
	}

	void AreaVisibility(float &currentFrame, float cooldown, int &health, float PlayerCoordinateX, float PlayerCoordinateY, float time, int MixAreaVisibilityX, int MixAreaVisibilityY, int MaxAreaVisibilityX, int MaxAreaVisibilityY, int StartingPositionX, int StartingPositionY) 
	{
		if (life) 
		{
			if ((PlayerCoordinateX < MaxAreaVisibilityX && PlayerCoordinateY < MaxAreaVisibilityY) && (MixAreaVisibilityX < PlayerCoordinateX && MixAreaVisibilityY < PlayerCoordinateY)) 
			{
				cooldownTime += time;
				battle(health, cooldown, PlayerCoordinateX, PlayerCoordinateY);
				update(currentFrame, time, PlayerCoordinateX, PlayerCoordinateY);
			}
			else
			{
				update(currentFrame, time, StartingPositionX, StartingPositionY);
			}
		}
	}

	void update(float &currentFrame, float time, float px, float py)
	{
		if (int(x) > int(px)) 
		{
			dx = -0.1; dy = 0;
			currentFrame += 0.005 * time;
			if (currentFrame > 4) currentFrame -= 4;
			sprite.setTextureRect(IntRect(260, 49 * int(currentFrame), w, h));
		}
		if (int(x) < int(px)) 
		{
			dx = 0.1; dy = 0;
			currentFrame += 0.005*time;
			if (currentFrame > 4) currentFrame -= 4;
			sprite.setTextureRect(IntRect(150, 49 * int(currentFrame), w, h));
		}
		if (int(y) > int(py)) 
		{
			dx = 0; dy = -0.1;
			currentFrame += 0.005 * time;
			if (currentFrame > 4) currentFrame -= 4;
			sprite.setTextureRect(IntRect(55, 49 * int(currentFrame), w, h));
		}
		if (int(y) < int(py)) 
		{
			dx = 0; dy = 0.1;
			currentFrame += 0.005 * time;
			if (currentFrame > 4) currentFrame -= 4;
			sprite.setTextureRect(IntRect(155, 49 * int(currentFrame), w, h));
		}
		if (int(y) == int(py) && int(x) == int(px)) 
		{
			dx = 0; dy = 0;
			sprite.setTextureRect(IntRect(260, 49, w, h));
		}
		x += dx * time;
		y += dy * time;
		sprite.setPosition(x, y);
		checkCollisionWithMap();
		if (health <= 0) { life = false; }
	}

	float getPlayerCoordinateX() { return x; }
	float getPlayerCoordinateY() { return y; }

	void checkCollisionWithMap()
	{
		for (int i = y / 32; i < (y + h) / 32; i++)
			for (int j = x / 32; j<(x + w) / 32; j++)
			{
				if (TileMap[i][j] == 's' || TileMap[i][j] == ',' ||
					TileMap[i][j] == 'c' || TileMap[i][j] == 'a' ||
					TileMap[i][j] == 'z' || TileMap[i][j] == 'l' ||
					TileMap[i][j] == '0' || TileMap[i][j] == 'o' ||
					TileMap[i][j] == '[' || TileMap[i][j] == ']' ||
					TileMap[i][j] == 'p' || TileMap[i][j] == ';' ||
					TileMap[i][j] == 'f' || TileMap[i][j] == 'j' ||
					TileMap[i][j] == '.' || TileMap[i][j] == '5' ||
					TileMap[i][j] == '!' || TileMap[i][j] == '?' ||
					TileMap[i][j] == '`' || TileMap[i][j] == '|' ||
					TileMap[i][j] == 'Q' || TileMap[i][j] == 'W' ||
					TileMap[i][j] == 'E' || TileMap[i][j] == 'T')
				{
					if (dy > 0) { y = i * 32 - h; }
					if (dy < 0) { y = i * 32 + 32; }
					if (dx > 0) { x = j * 32 - w; }
					if (dx < 0) { x = j * 32 + 32; }
				}
			}
	}

	void battle(int &healthP, float &cooldown, float Ex, float Ey) 
	{
		if (cooldownTime > cooldown) 
		{
			if ((int(Ex) - 20) <= int(getPlayerCoordinateX()) && (int(Ey) - 20) <= int(getPlayerCoordinateY()) && (int(Ex) + 20) >= int(getPlayerCoordinateX()) && (int(Ey) + 20) >= int(getPlayerCoordinateY())) 
			{
				healthP -= 25;
				cooldownTime = 0;
			}
		}
	}
};

class EnemyDead :public Entity 
{
public:
	float cooldownTime = 0;
	EnemyDead(int heal, String F, float X, float Y, int W, int H) :Entity(heal, F, X, Y, W, H) 
	{
		sprite.setTextureRect(IntRect(0, 0, w, h));
	}

	void AreaVisibility(float &currentFrame, float cooldown, int &health, float PlayerCoordinateX, float PlayerCoordinateY, float time, int MixAreaVisibilityX, int MixAreaVisibilityY, int MaxAreaVisibilityX, int MaxAreaVisibilityY, int StartingPositionX, int StartingPositionY) 
	{
		if (life) 
		{
			if ((PlayerCoordinateX < MaxAreaVisibilityX && PlayerCoordinateY < MaxAreaVisibilityY) && (MixAreaVisibilityX < PlayerCoordinateX && MixAreaVisibilityY < PlayerCoordinateY)) 
			{
				cooldownTime += time;
				battle(health, cooldown, PlayerCoordinateX, PlayerCoordinateY);
				update(currentFrame, time, PlayerCoordinateX, PlayerCoordinateY);
			}
			else
			{
				update(currentFrame, time, StartingPositionX, StartingPositionY);
			}
		}
	}

	void update(float &currentFrame, float time, float px, float py)
	{
		if (int(x) > int(px)) 
		{
			dx = -0.1; dy = 0;
			currentFrame += 0.005 * time;
			if (currentFrame > 4) currentFrame -= 4;
			sprite.setTextureRect(IntRect(50 * int(currentFrame), 48, w, h));
		}
		if (int(x) < int(px)) 
		{
			dx = 0.1; dy = 0;
			currentFrame += 0.005*time;
			if (currentFrame > 4) currentFrame -= 4;
			sprite.setTextureRect(IntRect(50 * int(currentFrame), 96, w, h));
		}
		if (int(y) > int(py)) 
		{
			dx = 0; dy = -0.1;
			currentFrame += 0.005 * time;
			if (currentFrame > 4) currentFrame -= 4;
			sprite.setTextureRect(IntRect(50 * int(currentFrame), 144, w, h));
		}
		if (int(y) < int(py)) 
		{
			dx = 0; dy = 0.1;
			currentFrame += 0.005 * time;
			if (currentFrame > 4) currentFrame -= 4;
			sprite.setTextureRect(IntRect(50 * int(currentFrame), 0, w, h));
		}
		if (int(y) == int(py) && int(x) == int(px)) 
		{
			dx = 0; dy = 0;
			sprite.setTextureRect(IntRect(0, 50, w, h));
		}
		x += dx * time;
		y += dy * time;
		sprite.setPosition(x, y);
		checkCollisionWithMap();
		if (health <= 0) { life = false; }
	}

	float getPlayerCoordinateX() { return x; }
	float getPlayerCoordinateY() { return y; }

	void checkCollisionWithMap()
	{
		for (int i = y / 32; i < (y + h) / 32; i++)
			for (int j = x / 32; j<(x + w) / 32; j++)
			{
				if (TileMap[i][j] == 's' || TileMap[i][j] == ',' ||
					TileMap[i][j] == 'c' || TileMap[i][j] == 'v' ||
					TileMap[i][j] == 'd' || TileMap[i][j] == 'a' ||
					TileMap[i][j] == 'z' || TileMap[i][j] == 'l' ||
					TileMap[i][j] == '0' || TileMap[i][j] == 'o' ||
					TileMap[i][j] == '[' || TileMap[i][j] == ']' ||
					TileMap[i][j] == 'p' || TileMap[i][j] == ';' ||
					TileMap[i][j] == 'f' || TileMap[i][j] == 'j' ||
					TileMap[i][j] == '.' || TileMap[i][j] == '5' ||
					TileMap[i][j] == '!' || TileMap[i][j] == '?' ||
					TileMap[i][j] == '`' || TileMap[i][j] == '|' ||
					TileMap[i][j] == 'Q' || TileMap[i][j] == 'W' ||
					TileMap[i][j] == 'E' || TileMap[i][j] == 'T')
				{
					if (dy > 0) { y = i * 32 - h; }
					if (dy < 0) { y = i * 32 + 32; }
					if (dx > 0) { x = j * 32 - w; }
					if (dx < 0) { x = j * 32 + 32; }
				}
			}
	}

	void battle(int &healthP, float &cooldown, float Ex, float Ey) 
	{
		if (cooldownTime > cooldown) 
		{
			if ((int(Ex) - 20) <= int(getPlayerCoordinateX()) && (int(Ey) - 20) <= int(getPlayerCoordinateY()) && (int(Ex) + 20) >= int(getPlayerCoordinateX()) && (int(Ey) + 20) >= int(getPlayerCoordinateY()))
			{
				healthP -= 30;
				cooldownTime = 0;
			}
		}
	}
};


int main()
{
	RenderWindow window(VideoMode(1280, 720), "The Game");
	menu(window);
	view.reset(FloatRect(0, 0, 640, 480));

	Music music;
	music.openFromFile("sounds/MusicAction.wav");
	music.play();
	music.setLoop(true);

	SoundBuffer kickBuffer;
	kickBuffer.loadFromFile("sounds/kick.ogg");
	Sound kick(kickBuffer);
	
        Image health_image;
	health_image.loadFromFile("images/health.png");
	Texture health;
	health.loadFromImage(health_image);
	Sprite s_health;
	s_health.setTexture(health);
	s_health.setTextureRect(IntRect(0, 0, 29, 49));
	s_health.setScale(0.6f, 0.6f);

	Font font;
	font.loadFromFile("CyrilicOld.ttf");

	Text health_text("", font, 10);
	health_text.setColor(Color::White);

	Text history_text("", font, 15);
	history_text.setColor(Color::Black);

	Image history_image;
	history_image.loadFromFile("images/history.jpg");
	Texture history_texture;
	history_texture.loadFromImage(history_image);
	Sprite s_history;
	s_history.setTexture(history_texture);
	s_history.setTextureRect(IntRect(0, 0, 340, 510));
	s_history.setScale(0.8f, 0.8f);

	Image map_image;
	map_image.loadFromFile("Images/map.png");
	Texture map;
	map.loadFromImage(map_image);
	Sprite s_map;
	s_map.setTexture(map);

	Player p(100, "hero.png", 250, 250, 48.0, 90.0);

	EnemyZombi E(100, "zombi.png", 625, 367, 26.0, 46.0);
	EnemyZombi E2(100, "zombi.png", 1797, 377, 26.0, 46.0);
	EnemyZombi E3(100, "zombi.png", 2048, 833, 26.0, 46.0);
	EnemyZombi E4(100, "zombi.png", 2243, 1027, 26.0, 46.0);
	EnemyZombi E5(100, "zombi.png", 2262, 1183, 26.0, 46.0);
	EnemyZombi E6(100, "zombi.png", 1994, 1408, 26.0, 46.0);
	EnemyZombi E7(100, "zombi.png", 2149, 1473, 26.0, 46.0);

	EnemyVolf V(150, "volf.png", 1107, 1194, 96.0, 96.0);

	EnemyKiller K(50, "killer.png", 1172, 96, 48.0, 49.0);
	EnemyKiller K2(50, "killer.png", 1121, 710, 48.0, 49.0);
	EnemyKiller K3(50, "killer.png", 2054, 128, 48.0, 49.0);
	EnemyKiller K4(50, "killer.png", 2320, 128, 48.0, 49.0);

	EnemyDead D(50, "death.png", 355, 1416, 50.0, 48.0);

	bool showHistoryText = true;
	float currentFrame = 0, currentFrame2 = 0;
	float cooldownP = 0, cooldownE = 0;

	Clock clock;

	while (window.isOpen())
	{
		float time = clock.getElapsedTime().asMicroseconds();
		clock.restart();
		time = time / 800;

		Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
			if (event.type == Event::KeyPressed)
				if (event.key.code == Keyboard::H && p.health_count > 0)
				{
					p.health_count--;
					p.health += 20;
				}
			if (event.type == Event::KeyPressed)
				if ((event.key.code == Keyboard::Tab))
				{
					switch (showHistoryText)
					{
					case true:
					{
						showHistoryText = false;
						p.life = false;
						break;
					}
					case false:
					{
						showHistoryText = true;
						p.life = true;
                                                if (p.episode == 6) p.life = false;
						break;
					}
					}
				}
		}

		if (p.life)
		{
			if (Keyboard::isKeyPressed(Keyboard::A) || Keyboard::isKeyPressed(Keyboard::Left))
			{
				p.dir = 1; p.speed = 0.1;
				currentFrame += 0.015 * time;
				if (currentFrame > 16) currentFrame -= 16;
				p.sprite.setTextureRect(IntRect(49 * int(currentFrame), 104, 44, 90));
			}
			if (Keyboard::isKeyPressed(Keyboard::D) || Keyboard::isKeyPressed(Keyboard::Right))
			{
				p.dir = 0; p.speed = 0.1;
				currentFrame += 0.015 * time;
				if (currentFrame > 16) currentFrame -= 16;
				p.sprite.setTextureRect(IntRect(49 * int(currentFrame), 5, 47, 90));
			}
			if (Keyboard::isKeyPressed(Keyboard::W) || Keyboard::isKeyPressed(Keyboard::Up))
			{
				p.dir = 3; p.speed = 0.1;
				currentFrame += 0.015 * time;
				if (currentFrame > 16) currentFrame -= 16;
				p.sprite.setTextureRect(IntRect(49 * int(currentFrame), 299, 48, 90));
			}
			if (Keyboard::isKeyPressed(Keyboard::S) || Keyboard::isKeyPressed(Keyboard::Down))
			{
				p.dir = 2; p.speed = 0.1;
				currentFrame += 0.015 * time;
				if (currentFrame > 16) currentFrame -= 16;
				p.sprite.setTextureRect(IntRect(49 * int(currentFrame), 202, 48, 90));
			}

			if (Keyboard::isKeyPressed(Keyboard::F))
			{
				cooldownP += time;
				p.battle(E.health, cooldownP, E.getPlayerCoordinateX(), E.getPlayerCoordinateY());
				p.battle(E2.health, cooldownP, E2.getPlayerCoordinateX(), E2.getPlayerCoordinateY());
				p.battle(E3.health, cooldownP, E3.getPlayerCoordinateX(), E3.getPlayerCoordinateY());
				p.battle(E4.health, cooldownP, E4.getPlayerCoordinateX(), E4.getPlayerCoordinateY());
				p.battle(E5.health, cooldownP, E5.getPlayerCoordinateX(), E5.getPlayerCoordinateY());
				p.battle(E6.health, cooldownP, E6.getPlayerCoordinateX(), E6.getPlayerCoordinateY());
				p.battle(E7.health, cooldownP, E7.getPlayerCoordinateX(), E7.getPlayerCoordinateY());

				p.battle(V.health, cooldownP, V.getPlayerCoordinateX(), V.getPlayerCoordinateY());

				p.battle(K.health, cooldownP, K.getPlayerCoordinateX(), K.getPlayerCoordinateY());
				p.battle(K2.health, cooldownP, K2.getPlayerCoordinateX(), K2.getPlayerCoordinateY());
				p.battle(K3.health, cooldownP, K3.getPlayerCoordinateX(), K3.getPlayerCoordinateY());
				p.battle(K4.health, cooldownP, K4.getPlayerCoordinateX(), K4.getPlayerCoordinateY());

				p.battle(D.health, cooldownP, D.getPlayerCoordinateX(), D.getPlayerCoordinateY());

				if (p.knife) kick.play();
			}


		}

		getPlayerCoordinateForView(p.getPlayerCoordinateX(), p.getPlayerCoordinateY());

		E.AreaVisibility(currentFrame2, 3000, p.health, p.getPlayerCoordinateX(), p.getPlayerCoordinateY(), time, 300, 64, 821, 710, 625, 367);
		E2.AreaVisibility(currentFrame2, 3000, p.health, p.getPlayerCoordinateX(), p.getPlayerCoordinateY(), time, 1612, 352, 1981, 390, 1797, 377);
		E3.AreaVisibility(currentFrame2, 3000, p.health, p.getPlayerCoordinateX(), p.getPlayerCoordinateY(), time, 1983, 750, 2320, 1540, 2048, 833);
		E4.AreaVisibility(currentFrame2, 3000, p.health, p.getPlayerCoordinateX(), p.getPlayerCoordinateY(), time, 1983, 750, 2320, 1540, 2243, 1027);
		E5.AreaVisibility(currentFrame2, 3000, p.health, p.getPlayerCoordinateX(), p.getPlayerCoordinateY(), time, 1983, 750, 2320, 1540, 2262, 1183);
		E6.AreaVisibility(currentFrame2, 3000, p.health, p.getPlayerCoordinateX(), p.getPlayerCoordinateY(), time, 1983, 750, 2320, 1540, 1994, 1408);
		E7.AreaVisibility(currentFrame2, 3000, p.health, p.getPlayerCoordinateX(), p.getPlayerCoordinateY(), time, 1983, 750, 2320, 1540, 2149, 1473);

		V.AreaVisibility(currentFrame2, 2000, p.health, p.getPlayerCoordinateX(), p.getPlayerCoordinateY(), time, 745, 1152, 1520, 1222, 1107, 1194);

		K.AreaVisibility(currentFrame2, 4000, p.health, p.getPlayerCoordinateX(), p.getPlayerCoordinateY(), time, 850, 64, 1486, 710, 1172, 96);
		K2.AreaVisibility(currentFrame2, 4000, p.health, p.getPlayerCoordinateX(), p.getPlayerCoordinateY(), time, 850, 64, 1486, 710, 1121, 710);
		K3.AreaVisibility(currentFrame2, 4000, p.health, p.getPlayerCoordinateX(), p.getPlayerCoordinateY(), time, 1983, 96, 2320, 750, 2054, 128);
		K4.AreaVisibility(currentFrame2, 4000, p.health, p.getPlayerCoordinateX(), p.getPlayerCoordinateY(), time, 1983, 96, 2320, 750, 2320, 128);

		D.AreaVisibility(currentFrame2, 2200, p.health, p.getPlayerCoordinateX(), p.getPlayerCoordinateY(), time, 32, 1312, 710, 1542, 355, 1416);

		p.update(time);
		window.setView(view);
		window.clear();

		for (int i = 0; i < HEIGHT_MAP; i++)
			for (int j = 0; j < WIDTH_MAP; j++)
			{
				if (TileMap[i][j] == 's') s_map.setTextureRect(IntRect(160, 32, 32, 32));
				if (TileMap[i][j] == ' ') s_map.setTextureRect(IntRect(160, 0, 32, 32));
				if (TileMap[i][j] == 'n') s_map.setTextureRect(IntRect(64, 32, 32, 32));
				if (TileMap[i][j] == 'm') s_map.setTextureRect(IntRect(96, 32, 32, 32));
				if (TileMap[i][j] == 'c') s_map.setTextureRect(IntRect(224, 64, 32, 32));
				if (TileMap[i][j] == 'g') s_map.setTextureRect(IntRect(288, 128, 32, 32));
				if (TileMap[i][j] == 'v') s_map.setTextureRect(IntRect(224, 96, 32, 32));
				if (TileMap[i][j] == 'd') s_map.setTextureRect(IntRect(224, 128, 32, 32));
				if (TileMap[i][j] == 'q') s_map.setTextureRect(IntRect(256, 0, 32, 32));
				if (TileMap[i][j] == 'w') s_map.setTextureRect(IntRect(288, 0, 32, 32));
				if (TileMap[i][j] == 'a') s_map.setTextureRect(IntRect(256, 32, 32, 32));
				if (TileMap[i][j] == 'z') s_map.setTextureRect(IntRect(288, 32, 32, 32));
				if (TileMap[i][j] == '1') s_map.setTextureRect(IntRect(256, 64, 32, 32));
				if (TileMap[i][j] == '2') s_map.setTextureRect(IntRect(288, 64, 32, 32));
				if (TileMap[i][j] == '3') s_map.setTextureRect(IntRect(256, 96, 32, 32));
				if (TileMap[i][j] == '4') s_map.setTextureRect(IntRect(288, 96, 32, 32));
				if (TileMap[i][j] == 'b') s_map.setTextureRect(IntRect(0, 32, 32, 32));
				if (TileMap[i][j] == 'x' || TileMap[i][j] == '0') s_map.setTextureRect(IntRect(64, 0, 32, 32));
				if (TileMap[i][j] == 'l' || TileMap[i][j] == '9') s_map.setTextureRect(IntRect(128, 64, 32, 32));
				if (TileMap[i][j] == 'h') s_map.setTextureRect(IntRect(96, 0, 32, 32));
				if (TileMap[i][j] == 'o' || TileMap[i][j] == '8') s_map.setTextureRect(IntRect(256, 128, 32, 32));
				if (TileMap[i][j] == 'u' || TileMap[i][j] == 'E') s_map.setTextureRect(IntRect(128, 96, 32, 32));
				if (TileMap[i][j] == '[') s_map.setTextureRect(IntRect(320, 0, 32, 32));
				if (TileMap[i][j] == ']') s_map.setTextureRect(IntRect(352, 0, 32, 32));
				if (TileMap[i][j] == 'p') s_map.setTextureRect(IntRect(320, 32, 32, 32));
				if (TileMap[i][j] == ';') s_map.setTextureRect(IntRect(352, 32, 32, 32));
				if (TileMap[i][j] == '/') s_map.setTextureRect(IntRect(0, 0, 32, 32));
				if (TileMap[i][j] == '`') s_map.setTextureRect(IntRect(192, 96, 32, 32));
				if (TileMap[i][j] == 'f') s_map.setTextureRect(IntRect(320, 64, 32, 32));
				if (TileMap[i][j] == 'j') s_map.setTextureRect(IntRect(320, 96, 32, 32));
				if (TileMap[i][j] == 't') s_map.setTextureRect(IntRect(320, 128, 32, 32));
				if (TileMap[i][j] == 'k') s_map.setTextureRect(IntRect(352, 64, 32, 32));
				if (TileMap[i][j] == 'e') s_map.setTextureRect(IntRect(352, 96, 32, 32));
				if (TileMap[i][j] == 'r') s_map.setTextureRect(IntRect(352, 128, 32, 32));
				if (TileMap[i][j] == 'y') s_map.setTextureRect(IntRect(384, 0, 32, 32));
				if (TileMap[i][j] == 'i') s_map.setTextureRect(IntRect(384, 32, 32, 32));
				if (TileMap[i][j] == ',') s_map.setTextureRect(IntRect(192, 128, 32, 32));
				if (TileMap[i][j] == '.') s_map.setTextureRect(IntRect(384, 64, 32, 32));
				if (TileMap[i][j] == '5') s_map.setTextureRect(IntRect(384, 96, 32, 32));
				if (TileMap[i][j] == '6') s_map.setTextureRect(IntRect(416, 0, 32, 32));
				if (TileMap[i][j] == '7') s_map.setTextureRect(IntRect(416, 32, 32, 32));
				if (TileMap[i][j] == '(') s_map.setTextureRect(IntRect(416, 64, 32, 32));
				if (TileMap[i][j] == ')') s_map.setTextureRect(IntRect(416, 96, 32, 32));
				if (TileMap[i][j] == '!') s_map.setTextureRect(IntRect(128, 0, 32, 32));
				if (TileMap[i][j] == '?') s_map.setTextureRect(IntRect(128, 32, 32, 32));
				if (TileMap[i][j] == '+') s_map.setTextureRect(IntRect(0, 160, 32, 32));
				if (TileMap[i][j] == '�') s_map.setTextureRect(IntRect(32, 160, 32, 32));
				if (TileMap[i][j] == '#') s_map.setTextureRect(IntRect(64, 160, 32, 32));
				if (TileMap[i][j] == '%') s_map.setTextureRect(IntRect(0, 192, 32, 32));
				if (TileMap[i][j] == '^') s_map.setTextureRect(IntRect(32, 192, 32, 32));
				if (TileMap[i][j] == ':') s_map.setTextureRect(IntRect(64, 192, 32, 32));
				if (TileMap[i][j] == '&') s_map.setTextureRect(IntRect(0, 224, 32, 32));
				if (TileMap[i][j] == '*') s_map.setTextureRect(IntRect(32, 224, 32, 32));
				if (TileMap[i][j] == '-') s_map.setTextureRect(IntRect(64, 224, 32, 32));
				if (TileMap[i][j] == '@') s_map.setTextureRect(IntRect(0, 64, 32, 32));
				if (TileMap[i][j] == '=') s_map.setTextureRect(IntRect(32, 64, 32, 32));
				if (TileMap[i][j] == '<') s_map.setTextureRect(IntRect(64, 64, 32, 32));
				if (TileMap[i][j] == '>') s_map.setTextureRect(IntRect(0, 96, 32, 32));
				if (TileMap[i][j] == '{') s_map.setTextureRect(IntRect(32, 96, 32, 32));
				if (TileMap[i][j] == '}') s_map.setTextureRect(IntRect(64, 96, 32, 32));
				if (TileMap[i][j] == '|') s_map.setTextureRect(IntRect(0, 128, 32, 32));
				if (TileMap[i][j] == 'Q') s_map.setTextureRect(IntRect(32, 128, 32, 32));
				if (TileMap[i][j] == 'W') s_map.setTextureRect(IntRect(64, 128, 32, 32));
				if (TileMap[i][j] == 'R') s_map.setTextureRect(IntRect(96, 96, 32, 32));
				if (TileMap[i][j] == 'T') s_map.setTextureRect(IntRect(224, 0, 32, 32));
				if (TileMap[i][j] == 'M') s_map.setTextureRect(IntRect(448, 96, 32, 32));
				if (TileMap[i][j] == 'H') s_map.setTextureRect(IntRect(416, 128, 32, 32));
				if (TileMap[i][j] == 'J') s_map.setTextureRect(IntRect(448, 64, 32, 32));
				s_map.setPosition(j * 32, i * 32);
				window.draw(s_map);
			}

		if (E.life)
		{
			Text texthealthE("", font, 10);
			texthealthE.setColor(Color::Red);
			std::ostringstream EnemyHealthString;
			EnemyHealthString << E.health;
			texthealthE.setString(EnemyHealthString.str());
			texthealthE.setPosition(E.getPlayerCoordinateX(), E.getPlayerCoordinateY() - 15);
			window.draw(texthealthE);
			window.draw(E.sprite);
		}

		if (E2.life)
		{
			Text texthealthE2("", font, 10);
			texthealthE2.setColor(Color::Red);
			std::ostringstream EnemyHealthString2;
			EnemyHealthString2 << E2.health;
			texthealthE2.setString(EnemyHealthString2.str());
			texthealthE2.setPosition(E2.getPlayerCoordinateX(), E2.getPlayerCoordinateY() - 15);
			window.draw(texthealthE2);
			window.draw(E2.sprite);
		}

		if (E3.life)
		{
			Text texthealthE3("", font, 10);
			texthealthE3.setColor(Color::Red);
			std::ostringstream EnemyHealthString3;
			EnemyHealthString3 << E3.health;
			texthealthE3.setString(EnemyHealthString3.str());
			texthealthE3.setPosition(E3.getPlayerCoordinateX(), E3.getPlayerCoordinateY() - 15);
			window.draw(texthealthE3);
			window.draw(E3.sprite);
		}

		if (E4.life)
		{
			Text texthealthE4("", font, 10);
			texthealthE4.setColor(Color::Red);
			std::ostringstream EnemyHealthString4;
			EnemyHealthString4 << E4.health;
			texthealthE4.setString(EnemyHealthString4.str());
			texthealthE4.setPosition(E4.getPlayerCoordinateX(), E4.getPlayerCoordinateY() - 15);
			window.draw(texthealthE4);
			window.draw(E4.sprite);
		}

		if (E5.life)
		{
			Text texthealthE5("", font, 10);
			texthealthE5.setColor(Color::Red);
			std::ostringstream EnemyHealthString5;
			EnemyHealthString5 << E5.health;
			texthealthE5.setString(EnemyHealthString5.str());
			texthealthE5.setPosition(E5.getPlayerCoordinateX(), E5.getPlayerCoordinateY() - 15);
			window.draw(texthealthE5);
			window.draw(E5.sprite);
		}

		if (E6.life)
		{
			Text texthealthE6("", font, 10);
			texthealthE6.setColor(Color::Red);
			std::ostringstream EnemyHealthString6;
			EnemyHealthString6 << E6.health;
			texthealthE6.setString(EnemyHealthString6.str());
			texthealthE6.setPosition(E6.getPlayerCoordinateX(), E6.getPlayerCoordinateY() - 15);
			window.draw(texthealthE6);
			window.draw(E6.sprite);
		}

		if (E7.life)
		{
			Text texthealthE7("", font, 10);
			texthealthE7.setColor(Color::Red);
			std::ostringstream EnemyHealthString7;
			EnemyHealthString7 << E7.health;
			texthealthE7.setString(EnemyHealthString7.str());
			texthealthE7.setPosition(E7.getPlayerCoordinateX(), E7.getPlayerCoordinateY() - 15);
			window.draw(texthealthE7);
			window.draw(E7.sprite);
		}

		if (V.life)
		{
			Text texthealthV("", font, 10);
			texthealthV.setColor(Color::Red);
			std::ostringstream EnemyHealthStringV;
			EnemyHealthStringV << V.health;
			texthealthV.setString(EnemyHealthStringV.str());
			texthealthV.setPosition(V.getPlayerCoordinateX() + 30, V.getPlayerCoordinateY() - 5);
			window.draw(texthealthV);
			window.draw(V.sprite);
		}
                else p.episode = 3;

		if (K.life)
		{
			Text texthealthK("", font, 10);
			texthealthK.setColor(Color::Red);
			std::ostringstream EnemyHealthStringK;
			EnemyHealthStringK << K.health;
			texthealthK.setString(EnemyHealthStringK.str());
			texthealthK.setPosition(K.getPlayerCoordinateX(), K.getPlayerCoordinateY() - 5);
			window.draw(texthealthK);
			window.draw(K.sprite);
		}

		if (K2.life)
		{
			Text texthealthK2("", font, 10);
			texthealthK2.setColor(Color::Red);
			std::ostringstream EnemyHealthStringK2;
			EnemyHealthStringK2 << K2.health;
			texthealthK2.setString(EnemyHealthStringK2.str());
			texthealthK2.setPosition(K2.getPlayerCoordinateX(), K2.getPlayerCoordinateY() - 5);
			window.draw(texthealthK2);
			window.draw(K2.sprite);
		}

		if (K3.life)
		{
			Text texthealthK3("", font, 10);
			texthealthK3.setColor(Color::Red);
			std::ostringstream EnemyHealthStringK3;
			EnemyHealthStringK3 << K3.health;
			texthealthK3.setString(EnemyHealthStringK3.str());
			texthealthK3.setPosition(K3.getPlayerCoordinateX(), K3.getPlayerCoordinateY() - 5);
			window.draw(texthealthK3);
			window.draw(K3.sprite);
		}

		if (K4.life)
		{
			Text texthealthK4("", font, 10);
			texthealthK4.setColor(Color::Red);
			std::ostringstream EnemyHealthStringK4;
			EnemyHealthStringK4 << K4.health;
			texthealthK4.setString(EnemyHealthStringK4.str());
			texthealthK4.setPosition(K4.getPlayerCoordinateX(), K4.getPlayerCoordinateY() - 5);
			window.draw(texthealthK4);
			window.draw(K4.sprite);
		}

                if (!K.life && !K2.life && !E.life) p.episode = 4;

		if (D.life)
		{
			Text texthealthD("", font, 10);
			texthealthD.setColor(Color::Red);
			std::ostringstream EnemyHealthStringD;
			EnemyHealthStringD << D.health;
			texthealthD.setString(EnemyHealthStringD.str());
			texthealthD.setPosition(D.getPlayerCoordinateX(), D.getPlayerCoordinateY() - 5);
			window.draw(texthealthD);
			window.draw(D.sprite);
		}
                else p.episode = 5;

                if (!E2.life && !E3.life && !E4.life && !E5.life && !E6.life && !E7.life && !K3.life && !K4.life) p.episode = 6;

		if (p.life)
		{
			Text texthealthP("", font, 20);
			texthealthP.setColor(Color::Green);
			std::ostringstream playerHealthString;
			playerHealthString << p.health;
			texthealthP.setString("��������:" + playerHealthString.str());
			texthealthP.setPosition(view.getCenter().x - 320, view.getCenter().y - 230);
			window.draw(texthealthP);
			window.draw(p.sprite);
			Text coordinate("", font, 14);
			coordinate.setColor(Color::Green);
			std::ostringstream coordinateString, coordinateString2;;
			coordinateString << int(p.x);
			coordinateString2 << int(p.y);
			coordinate.setString(coordinateString.str() + "  " + coordinateString2.str());
			coordinate.setPosition(view.getCenter().x - 320, view.getCenter().y - 200);
			window.draw(coordinate);
			s_health.setPosition(view.getCenter().x - 200, view.getCenter().y - 235);
			window.draw(s_health);
			std::ostringstream hl_text;
			hl_text << getTextHealth(p.health_count);
			health_text.setString(hl_text.str());
			health_text.setPosition(view.getCenter().x - 183, view.getCenter().y - 235);
			window.draw(health_text);
		}
		else 
                {
			Text Over("", font, 40);
			Over.setColor(Color::Red);
			Over.setString("���� ��������");
			Over.setPosition(view.getCenter().x - 140, view.getCenter().y - 50);
			window.draw(Over);
		}

		if (!showHistoryText)
		{
			std::ostringstream hs_text;
			hs_text << getTextHistory(p.episode);
			history_text.setString(hs_text.str());
			history_text.setPosition(view.getCenter().x - 140, view.getCenter().y - 190);
			s_history.setPosition(view.getCenter().x - 150, view.getCenter().y - 200);
			window.draw(s_history);
			window.draw(history_text);
		}

		window.display();
	}
	return 0;
}