#include <SFML/Graphics.hpp>
using namespace sf;

View view;

View getPlayerCoordinateForView(float x, float y) 
{
	float tempX = x; float tempY = y;
	if (x < 320) tempX = 320;
	if (x > 2050) tempX = 2050;
	if (y < 240) tempY = 240;
	if (y > 1390) tempY = 1390;
	view.setCenter(tempX, tempY);
	return view;
}
