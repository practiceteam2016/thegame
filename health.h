std::string getTextHealth(int health_count)
{
	std::string healthText = "0";
	if (health_count > 0)
	{
		if (health_count < 10)
			healthText = health_count + '0';
		else
		{
			int f_num = health_count / 10;
			int s_num = health_count % 10;
			healthText = f_num + '0';
			healthText += s_num + '0';
		}
	}
	return healthText;
}
